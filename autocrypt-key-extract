#!/usr/bin/python3

"""Scan an e-mail and try to extract the OpenPGP key from the
Autocrypt header and stuff it into GnuPG. Does not handle any other
autocrypt specification."""

import argparse
import email
import logging
import sys
import base64
import subprocess


def extract_header(stream):
    # parse email from stdin
    message = email.message_from_binary_file(stream)
    # extract Autocrypt header
    return message.get('Autocrypt', None)


def decode_header(header):
    # parse the base64 part
    try:
        keydata = base64.b64decode(header.split('keydata=')[1].strip().replace(' ', '').replace('\t', ''))
    except (base64.binascii.Error, IndexError) as e:
        logging.error("failure to parse Autocrypt header: %s" % e)
        return None
    return keydata


def openpgp_armor_data(data):
    # output the armored version
    return subprocess.run(['gpg', '--enarmor'], input=data).returncode


def gnupg_import_data(data):
    return subprocess.run(['gpg', '--import'], input=data).returncode


if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog=__doc__)
    subparsers = parser.add_subparsers(dest='action')
    subparsers.add_parser('dearmor', help="output binary key (default)")
    subparsers.add_parser('armor', help="ASCII-armor result, generally does not work")
    subparsers.add_parser('import', help="pipe binary output to gpg --import")
    args = parser.parse_args()
    logging.warning("use email-extract-openpgp-certs from mailscripts instead")

    header = extract_header(sys.stdin.buffer)
    if header is None:
        logging.error("no Autocrypt header found")
        sys.exit(1)
    data = decode_header(header)
    if data is None:
        sys.exit(2)
    if args.action == 'armor':
        sys.exit(openpgp_armor_data(data))
    elif args.action == 'import':
        sys.exit(gnupg_import_data(data))
    else:
        sys.stdout.buffer.write(data)

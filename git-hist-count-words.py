#!/usr/bin/python3
# coding: utf-8

"""count words in git repository"""

# sample git-hist-count-words.py -o ../o/ ; git co master
# graph-count-words.py -t ../o

# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
from datetime import datetime
import logging
import os
import subprocess
import sys


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__, epilog="""""")
    parser.add_argument(
        "--verbose",
        "-v",
        dest="log_level",
        action="store_const",
        const="info",
        default="warning",
    )
    parser.add_argument(
        "--debug",
        "-d",
        dest="log_level",
        action="store_const",
        const="debug",
        default="warning",
    )
    parser.add_argument("--output", "-o", help="output directory")
    parser.add_argument("--dryrun", "-n", action="store_true", help="do nothing")
    return parser.parse_args(args=args)


def main(args):
    if not args.dryrun:
        os.makedirs(args.output, exist_ok=True)
    logging.info("fetching revision list")
    p = subprocess.Popen(
        ["git", "rev-list", "--reverse", "--format=format:%H %aI", "HEAD"],
        stdout=subprocess.PIPE,
        text=True,
    )
    while True:
        line = p.stdout.readline()
        if not line:
            break
        line = line.strip()
        logging.debug("revlist: %s", line)
        if line.startswith("commit"):
            # git lists a "commit HASH" line that we do not need
            continue
        rev, datestr = line.split(" ", 2)
        date = datetime.fromisoformat(datestr)

        logging.info("checking out revision %s from %s", rev, date)
        subprocess.check_call(["git", "checkout", "--quiet", rev])

        outpath = args.output + "/" + datestr + "-" + rev
        logging.info("writing word count to %s", outpath)
        with open(outpath, "w+b") as output:
            # find * -type f -name '*.md' -print0 | wc --files0-from=- -w | tail -1
            find = subprocess.Popen(
                # fmt: off
                [
                    "find",
                    "(", "-path", "./.git", "-prune", ")",
                    "-o",
                    "(",
                    "-type", "f",
                    "-a",
                    "(",
                    "-iname", "*.md",
                    "-o",
                    "-iname", "*.mdwn",
                    "-o",
                    "-iname", "*.creole",
                    ")",
                    "-a",
                    "-print0",
                    ")",
                ],
                # fmt: on
                stdout=subprocess.PIPE,
            )
            wc = subprocess.Popen(
                ["wc", "--files0-from=-", "-w"], stdin=find.stdout, stdout=output
            )
            find.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
            out, err = wc.communicate()
            logging.debug("out: %s, err: %s", out, err)


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(format="%(message)s", level=args.log_level.upper())
    main(args)

#!/bin/sh

set -e

echo "use borgmatic instead, this backs up to external USB drive"

printf "make sure the drive is powered, hit enter to continue: "
read junk

# simulates what Thunar/pmount does
# see also https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=807872
uuid=5f13a648-7770-446b-928c-21a7393d3db6
dev="/dev/disk/by-id/dm-name-luks-$uuid"
if ! [ -e "$dev" ]; then
    cryptsetup luksOpen "/dev/disk/by-uuid/$uuid" "luks-$uuid"
    LABEL=$(blkid -o value -s LABEL "$dev")
    mkdir -p "/media/$LABEL"
    mount "$dev" "/media/$LABEL"
fi
LABEL=$(blkid -o value -s LABEL "$dev")

finish() {
    umount "/media/$LABEL"
    cryptsetup luksClose "luks-$uuid"
}

BORG_REPO="/media/$LABEL/borg-$(hostname)"
export BORG_REPO

trap finish INT QUIT HUP TERM

echo "run this as the user anarcat"
echo 'for annex in video mp3 books Photos; do git -C /home/anarcat/$annex annex sync --content; done'

BORG_OPTS='--exclude-caches --keep-tag-files --one-file-system'
BORG_OPTS="$BORG_OPTS --exclude-if-present .nobackup "
BORG_OPTS="$BORG_OPTS --verbose --stats --progress"

borg create $BORG_OPTS \
     -e /home/anarcat/mp3/ \
     -e /home/anarcat/Music/ \
     -e /home/anarcat/video/ \
     -e /home/anarcat/Photos/ \
     -e /home/anarcat/Photos.local/ \
     -e /home/anarcat/iso/ \
     -e /home/anarcat/isos/ \
     -e /home/anarcat/books \
     -e /home/anarcat/books-incoming \
     -e /home/anarcat/dist \
     -e /home/anarcat/VirtualMachines \
     -e '/home/anarcat/VirtualBox VMs' \
     -e '/home/*/.cache/' \
     -e '*/.Trash-*/' \
     -e '*/.bitcoin/blocks/' \
     -e '*/build-area/*' \
     -e "/var/cache/*" \
     -e "/tmp/*" \
     -e "/var/tmp/*" \
     -e /srv/chroot \
     ::"{hostname}-{now}" /

# keep 10 years, 3 months and all backups in the last 7 days
#
# we can't use -d7 here because we don't backup daily. it would count
# backups going way too far back because it "pushes" the other backup
# types back, as "backups selected by previous rules do not count
# towards those of later rules".
borg prune --stat --list -y 10 -m 3 --keep-within 7d --prefix '{hostname}-'

finish

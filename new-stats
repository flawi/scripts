#!/usr/bin/python3

import argparse
from collections import defaultdict
from datetime import datetime
import email.parser
import fileinput
import socket
import sys


def parse_mailbox(files):
    parser = email.parser.FeedParser()

    with fileinput.input(files) as f:
        for line in f:
            parser.feed(line)
            if not line.strip():
                # end of message
                yield parser.close()
                parser = email.parser.FeedParser()


def filter_pkgs(packages, queue):
    for package in packages:
        if package['Queue'] == queue:
            yield package


def field_stats(packages, fields):
    stats = {}
    for field in fields:
        stats[field] = defaultdict(int)
    for package in packages:
        for field in fields:
            if package[field] is None:
                continue
            stats[field][package[field]] += 1
    return stats


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--queue', default='new', metavar='QUEUE',
                        help='print stats for QUEUE (default: %(default)s)',
                        choices=['new', 'backports-new',
                                 'oldstable-new', 'stable-new'])
    parser.add_argument('--count', '-c', default=10, type=int)
    FIELDS = ('Maintainer', 'Sponsored-by', 'Changed-by')
    parser.add_argument('path', nargs='*', default='-')
    args = parser.parse_args()

    packages = list(filter_pkgs(parse_mailbox(args.path), args.queue))
    stats = field_stats(packages, FIELDS)

    title = '''FTP masters queue analysis'''
    print(title)
    print('=' * len(title))

    total = len(list(packages))
    print()
    print('Total for queue "{}": {}'.format(args.queue, total))

    for field in FIELDS:
        print()
        title = "Top {} ({})".format(args.count, field)
        print(title)
        print('-' * len(title))
        print()

        stat = reversed(sorted(stats[field].items(), key=lambda kv: kv[1]))
        for key, val in list(stat)[:args.count]:
            print("{:>3.0%} {:>4} {}".format(val/total, val, key))

    print()
    source_data = 'https://ftp-master.debian.org/new.822'
    source_code = 'https://gitlab.com/anarcat/scripts/blob/master/new-stats'
    print("Report generated from {}:{} at {} from {}.".format(socket.getfqdn(),
                                                              sys.argv[0],
                                                              datetime.now(),
                                                              source_data))
    print('Source code available at: {}'.format(source_code))


if __name__ == '__main__':
    main()

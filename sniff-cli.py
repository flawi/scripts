#!/usr/bin/python3

import datetime
import psutil

t = datetime.datetime.now()
i = 0
while True:
    i += 1
    delta = (datetime.datetime.now() - t).seconds
    if delta:
        print("\r%d loops per second" % (i/int(delta)))
        t = datetime.datetime.now()
        i = 0
    for proc in psutil.pids():
        try:
            cmdline = psutil._psplatform.Process(proc).cmdline()
            d = [x for x in cmdline if 'RESTIC_PASSWORD' in x]
            if d:
                print(cmdline)
        except psutil.NoSuchProcess:
            pass
